// 1. What directive is used by Node.js in loading the modules it needs?
/*
	-To import "require" module like (const http = require('http'))
*/
// 2. What Node.js module contains a method for server creation?
/*
	-The http module contains "function" as an argument and allows server creation. The arguments passed in the function are request and response. 
*/
// 3. What is the method of the http object responsible for creating a server using Node.js?
/*
	-To create a server using node.js this code can be used:
	1.const server = http;
	2.const port = 3000;
	3.const server = http.createServer(function(request, response){
	response.writeHead(200,{'Content-Type':'text/plain'});
	response.end('Example Message')
	})
	4.server.listen(port)
	5.console.log('Server running now');
*/
// 4. What method of the response object allows us to set status codes and content types?
/*
	-writeHead()
*/
// 5. Where will console.log() output its contents when run in Node.js?
/*
	-We can see the console.log() output when we run it in our git bash or in other terminal when we run our node.js.
*/
// 6. What property of the request object contains the address's endpoint?
/*
	-The request.url
*/

const http=require('http')

let url = require("url")

const port = 3000

const server = http.createServer((request, response) => {
	if(request.url == '/login'){
		response.writeHead(200, {'Content-Type':'text/plain'})
		response.end('Welcome to the login page')
	}
	else{
		response.writeHead(400, {'Content-Type':'text/plain'})
		response.end("I'm sorry the page you are looking for cannot be found.")
	}
})

server.listen(port)


console.log('Server is successfully running.');